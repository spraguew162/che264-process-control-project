import math
from time import time

class TestProcess:
    def __init__(self,dt = 0.1):
        self.start_time = time()

    def __call__(self,x=None):
        if x is None:
            t = time() - self.start_time
            output = 60 + 10 * math.exp(-0.5 * t) + 2 * math.exp(-0.1*t) * math.sin(4 * t)
            return output
    
    def test(self):
        x = self(1,10)
        print(x)