# CHE264 Process Control Project

Requirements:
    Python 3.X

Requirements for Raspberry PI:
    RPIO (for run_process_control)
    simple_pid (for PID control)

Requirements for desktop machine:
    pyspectator (for send_temperature_to_controller)
        psutil
        netifaces
        wmi (only on Windows)
        enum34 (only on python 3.0.0 - 3.4.0)
