from sys import argv
import os
import time
#from pyspectator.processor import Cpu
import sensors

# Function to push the current temperature as a .txt file to the controller
def push_to_server(temp):
    global origin_file_name, ssh_address
    current_temperature = temp
    print("Current temperature is: " + str(current_temperature))
    with open(origin_file_name, 'w') as f:
        f.write(str(current_temperature))
    os.system("scp {} {}:{}".format(origin_file_name,ssh_address,destination_folder_name))

def get_temperature():
    sensors.init()
    try:
        tempCum = 0
        for chip in sensors.iter_detected_chips():
            #print("{} at {}".format(chip, chip.adapter_name))
            for feature in chip:
                #print("  {}: {}".format(feature.label, feature.get_value()))
                if feature.label[0:4] == "Core":
                    tempCum += float(feature.get_value())
        return tempCum / 4
    finally:
        sensors.cleanup()

# Loading the command line argument as the polling time in seconds
if len(argv) > 1:
    polling_time = int(argv[1])
else:
    polling_time = 5

# Static location that should not be changing.
ssh_address = "pi@192.168.1.30"

# Defining filename with absolute filepath for later.
origin_file_name = os.getcwd() + "/current_temperature.txt"
destination_folder_name = "/home/pi/Desktop/che264-process-control-project"

#cpu = Cpu(monitoring_latency=1)

while True:
    temp = get_temperature()
    push_to_server(temp)
    time.sleep(polling_time)



#with cpu:
#    while True:
#        temp = cpu.temperature
#        push_to_server(temp)
#        time.sleep(polling_time)


