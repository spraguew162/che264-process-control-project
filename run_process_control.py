"""Primary script for Raspberry Pi to run process control."""

from sys import argv
import process_control as pc
 

def main():
    """Runs basic process control algorithm."""

    # load command line selection
    if len(argv) > 1:
        config = argv[1]
    else:
        config = "config.csv"

    pc.processControl(config)

main()
