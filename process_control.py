"""Contains mathematical helper functions for Raspberry PI to run process control."""

import pwm_control_out
from time import time, sleep
from datetime import datetime
from sys import argv
from simple_pid import PID # not yet working
import random

def writeToLog(s,log="log.txt"):
    """Prints a string and outputs to a logfile.
    Arguments:
        s: The string to be output.
        log: The filepath for the log file.
    """
    s = str(datetime.now()) + "    " + s
    print(s)
    with open(log,"a") as f:
        f.write(s+"\n")

def writeToPWM(out):
    """Writes current output to PWM control.
    Arguments:
        out: The quantity to be sent to the PWM controller."""

    pass

def readFromTempFile():
    """Reads input from CPU temperature.
    Returns:
        x: Value of input."""
    with open("./current_temperature.txt","r") as f:
        temp = f.readline().strip()
        if temp == "None":
            temp = "1000"
        temp = float(temp)
    return temp

def writeCSV(path,*l,new=False):
    """Writes values to a CSV.
    Arguments:
        path: filepath to be written to
        *l: Iterables which contain rows for .csv file."""
    if new:
        editcode = "w"
    else:
        editcode = "a"
    with open(path,editcode) as f:
        for row in l:
            for item in row:
                f.write(str(item) + ",")
        f.write("\n")

def simple_pid_loop(config,r=readFromTempFile,w=writeToPWM,log=writeToLog):
    """Uses the simple_pid controller module to perform PID control.
    Arguments:
        config: Configuration parameters for control loop in dictionary form.
        r: The reader function (reads from arbitrary input).
        w: The writer function (writes to controller output).
        log: The logging function."""
    
    config_init_message = "Simple PID being initialized with configuration {}.".format(str(config))
    log(config_init_message)

    if "Kp" in config:
        Kp = float(config["Kp"])
    else:
        Kp = -1.0
    
    if "Kd" in config:
        Kd = float(config["Kd"])
    else:
        Kd = 0.0
    
    if "Ki" in config:
        Ki = float(config["Ki"])
    else:
        Ki = 0.0
    
    if "runtime" in config:
        runtime = float(config["runtime"])
    else:
        runtime = 3600
    
    if "sample_time" in config:
        sample_time = float(config["sample_time"])
    else:
        sample_time = 0.01
    
    if "delay_time" in config:
        delay_time = float(config["delay_time"])
    else:
        delay_time = 1.0
    
    if "set_point" in config:
        set_point = float(config["set_point"])
    else:
        set_point = 40.0
    
    if "out_file" in config:
        out_file = config["out_file"]
    else:
        out_file = "out.csv"

    if "out_low" in config:
        out_low = config["out_low"]
    else:
        out_low = 0.0

    if "out_high" in config:
        out_high = config["out_high"]
    else:
        out_high = 3.3
    
    # initialize controller
    start_time = time()
    controller = PID(Kp=Kp,Kd=Kd,Ki=Ki,sample_time=sample_time)
    controller.output_limits = (out_low, out_high)
    w = pwm_control_out.PWMController(PIDbounds=(out_low,out_high))
    log("PID initialization successful.")

    # start outfile
    writeCSV(out_file,["Time","Input","Output"],new=True)
    log("Output file loaded successfully.")

    n = 0
    while time() <= start_time + runtime:
        n += 1
        inputVal = r()
        outputVal = controller(inputVal - set_point)
        w(outputVal)
        log("Input: {0!s} C --> Output: {1!s} V".format(round(inputVal,2),round(outputVal,3)))
        writeCSV(out_file,[str(time() - start_time),str(inputVal),str(outputVal)])
        sleep(max(delay_time * n - time() + start_time,0))

    log("PID run complete.  Ending loop.")
    return

def custom_pid_loop(Kp=0,Kd=0,Ki=0,r=readFromTempFile,w=writeToPWM,log=writeToLog):
    """Uses a custom controller function to perform PID control.
    Arguments:
        Kp: Proportional control gain.
        Kd: Derivative control gain.
        Ki: Integral control gain.
        r: The reader function (reads from arbitrary input).
        w: The writer function (writes to controller output).
        log: The logging function."""
    
    pass

def loadParamsFromCSV(config):
    """Reads parameter settings from CSV file, returns as dictionary of list.
    Arguments:
        config: filepath for CSV file.
    Returns:
        params: dictionary with headers as keys and list of corresponding column as values."""

    with open(config,"r") as f:
        lines = f.readlines()
    
    headers = lines[0].rstrip("\n").split(",")
    params = {}
    for header in headers:
        params[header] = []
    params["headers"] = headers
    for i in range(1,len(lines)):
        row = lines[i].rstrip("\n").split(",")
        for j in range(len(headers)):
            header = headers[j]
            if j >= len(row):
                val = ""
            else:
                val = row[j]
            col = params[header]
            col.append(val)
    
    return params

def processControl(config,r=readFromTempFile,w=writeToPWM,log=writeToLog):
    """Main control loop for process control.
    Arguments:
        config: The control loop configuration.
        r: The reader function (reads from arbitrary input).
        w: The writer function (writes to controller output).
        log: The logging function."""

    init_message = "Process control started from config file {0:s}".format(config)
    log(init_message)

    paramsFromFile = loadParamsFromCSV(config)

    if len(argv) >= 2 and argv[2].isdigit():
        param_row = int(argv[2])
    else:
        param_row = 0

    params = {}
    for header in paramsFromFile["headers"]:
        valsColumn = paramsFromFile[header]
        params[header] = valsColumn[param_row]

    simple_pid_loop(params,r=r,w=w)
