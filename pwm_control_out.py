"""Contains interface functions to allow Raspberry PI to write PWM and control output."""

from RPi import GPIO as IO 


class PWMController():
    def __init__(self,PIDbounds=(-1,1)):
        IO.setmode(IO.BOARD)
        self.subcycle_frequency = 500
        self.fan_pins = (12, 18) #  set to 2 pins for later if we need to
        self.max_PID = PIDbounds[1]
        self.min_PID = PIDbounds[0]
        self._initialize_PWM()
        pass

    def _initialize_PWM(self):
        IO.setup(self.fan_pins[0], IO.OUT)
        IO.setup(self.fan_pins[1], IO.OUT)
        PWM_fan_1 = IO.PWM(self.fan_pins[0], self.subcycle_frequency)
        PWM_fan_2 = IO.PWM(self.fan_pins[1], self.subcycle_frequency)
        PWM_fan_1.start(100)
        PWM_fan_2.start(100)
        self.PWMouts = (PWM_fan_1,PWM_fan_2)

    def _PID_to_PWM(self,pid_output):
        if pid_output > self.max_PID:
            return 100
        elif pid_output < self.min_PID:
            return 0
        pwm_duty = (pid_output - self.min_PID)/(self.max_PID - self.min_PID) * 100
        return pwm_duty

    def _writeToPWM(self,pid_output):
        duty_cycle = self._PID_to_PWM(pid_output)
        for i in range(len(self.PWMouts)):
            output = self.PWMouts[i]
            output.ChangeDutyCycle(duty_cycle)

    def __call__(self,x):
        #convert signal to PWM
        output = self._PID_to_PWM(x)
        #write to PWM
        self._writeToPWM(output)
    def __exit__(self):
        IO.cleanup()

def PWMreader(filepath="default_file_path.txt"):
    with open(filepath,"r") as f:
        input = float(f.readline().rstrip("\n"))
    return input